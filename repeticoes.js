console.log("repetição while");
let numero =0;
while(numero<=10){
    console.log(`Valor nr: ${(numero)}`);
    if(numero%2==0){
        console.log(`Valor nr: ${(numero)} é par`);
    }
    numero++;
}
let numero1 = 0;
console.log("Repetição Do/While");
do{
    console.log(`Valor nr: ${(numero1)}`);
    numero1++;
}while(numero1<=10);

console.log("Repetição For");
for(let numero2=0; numero2<=10; numero2++){
    console.log(`Valor nr: ${(numero2)}`);
}